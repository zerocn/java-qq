package org.zero.view;

import java.awt.EventQueue;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class ServerApp {
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel("org.jvnet.substance.skin.SubstanceChallengerDeepLookAndFeel");
			} catch (Exception e) {
				SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "加载皮肤错误\n" + e.getMessage(), "错误",
						JOptionPane.WARNING_MESSAGE));
			}
			ServerWindow.getInstance();
		});
	}
}
