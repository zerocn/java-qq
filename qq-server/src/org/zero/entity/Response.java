package org.zero.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class Response implements Serializable {
	private static final long serialVersionUID = -1668424644205178126L;

	public static final int LOGIN_SUCCESS = 100;
	public static final int LOGIN_USERNAME_INVALID = 101;
	public static final int LOGIN_PASSWORD_ERROR = 102;

	public static final int REGISTER_SUCCESS = 200;
	public static final int REGISTER_FAIL = 201;

	public static final int ADDFRIEND_SUCCESS = 300;
	public static final int ADDFRIEND_FAIL = 301;
	public static final int ADDFRIEND_FAIL_ISFRIEND = 302;

	private String responseServiceName;
	private int responseCode;
	private User fromUser;
	private User toUser;
	private List<User> friends;
	private String message;
}
