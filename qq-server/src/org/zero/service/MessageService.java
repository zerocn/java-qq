package org.zero.service;

import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

import org.zero.dao.ServerRunStatus;
import org.zero.entity.Request;
import org.zero.entity.Response;
import org.zero.entity.User;
import org.zero.view.ServerWindow;

public class MessageService implements ServerService {

	@Override
	public void service(Request request, Socket socket, ServerWindow serverWindow) throws IOException {
		User fromUser = request.getFromUser();
		User toUser = request.getToUser();
		String massage = request.getMessage();
		if (Objects.nonNull(fromUser) && Objects.nonNull(toUser)) {
			Response response = new Response();
			response.setResponseServiceName(request.getRequestServiceName());
			response.setMessage(massage);
			response.setFromUser(fromUser);
			response.setToUser(toUser);
			send(ServerRunStatus.getSocket(toUser), response);
			serverWindow.println("用户【" + fromUser.getNickname() + "-" + fromUser.getId() + "】给用户【" + fromUser.getNickname()
					+ "-" + fromUser.getId() + "】发送了信息");
		}
	}
}
