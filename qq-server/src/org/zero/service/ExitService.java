package org.zero.service;

import java.io.IOException;
import java.net.Socket;

import org.zero.dao.ServerRunStatus;
import org.zero.entity.Request;
import org.zero.entity.User;
import org.zero.view.ServerWindow;

public class ExitService implements ServerService {

	@Override
	public void service(Request request, Socket socket, ServerWindow serverWindow) throws IOException {
		User user = request.getFromUser();
		ServerRunStatus.removeSocket(user);
		user.setOnline(false);
		socket.close();
		serverWindow.println("用户【" + user.getNickname() + "-" + user.getName() + "-" + user.getId() + "】已下线");
	}
}
