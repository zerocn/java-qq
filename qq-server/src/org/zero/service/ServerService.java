package org.zero.service;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.zero.entity.Request;
import org.zero.entity.Response;
import org.zero.view.ServerWindow;

public interface ServerService {
	void service(Request request, Socket socket, ServerWindow serverWindow) throws IOException;

	default void send(Socket socket, Response response) throws IOException {
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		objectOutputStream.writeObject(response);
		objectOutputStream.flush();
	}
}
