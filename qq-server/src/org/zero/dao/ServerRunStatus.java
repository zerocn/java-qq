package org.zero.dao;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.zero.entity.User;

public class ServerRunStatus {

	private static Map<User, Socket> userSocket = new HashMap<>(16);

	public static Socket getSocket(User user) {
		return userSocket.get(user);
	}

	public static void setSocket(User user, Socket socket) {
		userSocket.put(user, socket);
	}

	public static void removeSocket(User user) {
		userSocket.remove(user);
	}

	public static void clearSocket() {
		userSocket.clear();
	}
}
