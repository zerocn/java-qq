package org.zero.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

public class ObjectAppendInputStream extends ObjectInputStream {

	public ObjectAppendInputStream(InputStream in) throws IOException {
		super(in);
	}

	@Override
	protected void readStreamHeader() throws IOException, StreamCorruptedException {
		// 重写读取头部信息方法：什么也不做
	}
}