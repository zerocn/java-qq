package org.zero.monitor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.zero.entity.Request;
import org.zero.entity.User;
import org.zero.util.Connection;
import org.zero.view.LoginWindow;
import org.zero.view.RegisterWindow;

//完成登录窗口的监听
public class LoginMonitor implements ActionListener {

	private LoginWindow loginWindow;

	public LoginMonitor(LoginWindow loginWindow) {
		this.loginWindow = loginWindow;
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		String actionCommand = actionEvent.getActionCommand();
		String id = loginWindow.getId();
		String password = loginWindow.getPassword();

		if ("Login".equals(actionCommand)) {
			if (id.isBlank() || password.isBlank()) {
				SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(loginWindow, "QQ号或密码为空！！！", "内容为空",
						JOptionPane.WARNING_MESSAGE));
				return;
			}

			// 封装 User
			User user = new User();
			user.setId(id);
			user.setPassword(password);

			// 封装 Request
			Request request = new Request();
			request.setRequestServiceName("Login");
			request.setFromUser(user);

			Connection.send(request);
			Connection.receive(loginWindow);

		}

		if ("Register".equals(actionCommand)) {
			loginWindow.dispose();
			new RegisterWindow();
		}

	}

}
