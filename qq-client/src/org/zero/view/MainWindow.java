package org.zero.view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.zero.entity.Request;
import org.zero.entity.User;
import org.zero.util.Connection;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = -4985558020713428247L;

	private User user;

	public MainWindow(User user) {
		this.user = user;

		setSize(500, 500);
		// 在屏幕中居中显示
		setLocationRelativeTo(null);
		// 禁止缩放
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		setTitle(user.getNickname() + "-" + user.getName() + "（" + user.getId() + "）");
		try {
			setIconImage(ImageIO.read(new File("res/Icon.jpg")));
		} catch (IOException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(this, "图标文件缺失\n" + e.getMessage(), "错误",
					JOptionPane.WARNING_MESSAGE));
		}

		init();
		setLayout();
		addListener();
		setVisible(true);
	}

	private void init() {

	}

	private void setLayout() {

	}

	private void addListener() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Request request = new Request();
				request.setRequestServiceName("Exit");
				request.setFromUser(user);
				Connection.send(request);
				System.exit(0);
			}

		});
	}

}
