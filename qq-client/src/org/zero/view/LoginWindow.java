package org.zero.view;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.zero.monitor.LoginMonitor;

public class LoginWindow extends JFrame {
	private static final long serialVersionUID = -8123489772347729069L;

	private JLabel titleLabel;
	private JLabel idLabel;
	private JLabel passwordLabel;

	private JTextField idTextField;
	private JPasswordField passwordTextField;

	private JButton loginButton;
	private JButton registerButton;

	public String getId() {
		return idTextField.getText().trim();
	}

	public void setId(String id) {

		idTextField.setText(id.trim());

	}

	public String getPassword() {
		return new String(passwordTextField.getPassword()).trim();
	}

	public void setPassword(String password) {

		passwordTextField.setText(password.trim());

	}

	public LoginWindow() {
		setSize(430, 210);
		// 在屏幕中居中显示
		setLocationRelativeTo(null);
		// 禁止缩放
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setTitle("QQ 客户端-登录");
		try {
			setIconImage(ImageIO.read(new File("res/Icon.jpg")));
		} catch (IOException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(this, "图标文件缺失\n" + e.getMessage(), "错误",
					JOptionPane.WARNING_MESSAGE));
		}

		init();
		setLayout();
		addListener();
		setVisible(true);
	}

	public LoginWindow(String id) {
		this();
		setId(id);
	}

	private void init() {
		titleLabel = new JLabel("欢迎登录");
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setFont(new Font("楷体", Font.BOLD, 28));

		idLabel = new JLabel("QQ号：");
		idLabel.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 12));
		idTextField = new JTextField();

		passwordLabel = new JLabel("密 码：");
		passwordLabel.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 12));
		passwordTextField = new JPasswordField();

		loginButton = new JButton("登  录");
		loginButton.setFont(new Font("黑体", Font.PLAIN, 12));
		registerButton = new JButton("注  册");
		registerButton.setFont(new Font("黑体", Font.PLAIN, 12));
	}

	private void setLayout() {
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(loginButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 212, Short.MAX_VALUE).addComponent(
										registerButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(passwordLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(idLabel, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
								.addPreferredGap(ComponentPlacement.RELATED).addGroup(
										groupLayout.createParallelGroup(Alignment.TRAILING)
												.addComponent(passwordTextField, GroupLayout.DEFAULT_SIZE, 351,
														Short.MAX_VALUE)
												.addComponent(idTextField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
														351, Short.MAX_VALUE))))
				.addContainerGap())
				.addComponent(titleLabel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addComponent(titleLabel, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(idTextField, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
						.addComponent(idLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(passwordTextField, GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE).addComponent(
								passwordLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(loginButton, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
						.addComponent(registerButton, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
				.addGap(9)));
		getContentPane().setLayout(groupLayout);
	}

	private void addListener() {
		loginButton.setActionCommand("Login");
		loginButton.addActionListener(new LoginMonitor(this));

		registerButton.setActionCommand("Register");
		registerButton.addActionListener(new LoginMonitor(this));

		idTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					passwordTextField.grabFocus();
				}
			}
		});

		passwordTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					loginButton.doClick();
				}
			}
		});
	}
}
