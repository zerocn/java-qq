package org.zero.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.zero.monitor.RegisterMonitor;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class RegisterWindow extends JFrame {
	private static final long serialVersionUID = -6989743851060654750L;

	private JLabel titleLabel;
	private JLabel messageLabel;
	private JLabel nameLabel;
	private JLabel nicknameLabel;
	private JLabel passwordLabel;
	private JLabel confirmPasswordLabel;

	private JTextField nameTextField;
	private JTextField nicknameTextField;
	private JPasswordField passwordField;
	private JPasswordField confirmPasswordField;

	private JButton resetButton;
	private JButton registerButton;

	public void setMessage(String message) {
		messageLabel.setText(message.trim());
	}

	public void setName(String username) {
		nameTextField.setText(username.trim());
	}

	public void setNickname(String nickname) {
		nicknameTextField.setText(nickname.trim());

	}

	public void setPassword(String password) {
		passwordField.setText(password.trim());

	}

	public void setConfirmPassword(String confirmPassword) {
		confirmPasswordField.setText(confirmPassword.trim());

	}

	public String getName() {
		return nameTextField.getText().trim();

	}

	public String getNickname() {
		return nicknameTextField.getText().trim();
	}

	public String getPassword() {
		return new String(passwordField.getPassword()).trim();
	}

	public String getConfirmPassword() {
		return new String(confirmPasswordField.getPassword()).trim();

	}

	public RegisterWindow() {
		setSize(500, 326);
		setLocationRelativeTo(null); // 在屏幕中居中显示
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setTitle("QQ 客户端-注册");
		try {
			setIconImage(ImageIO.read(new File("res/Icon.jpg")));
		} catch (IOException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(this, "图标文件缺失\n" + e.getMessage(), "错误",
					JOptionPane.WARNING_MESSAGE));
		}

		init();
		setLayout();
		addListener();
		setVisible(true);
	}

	private void init() {
		titleLabel = new JLabel("欢迎注册");
		titleLabel.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 16));
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);

		messageLabel = new JLabel("");
		messageLabel.setForeground(new Color(0, 191, 255));
		messageLabel.setFont(new Font("Serif", Font.BOLD, 12));
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);

		nameLabel = new JLabel("用 户 名：");
		nameTextField = new JTextField();

		nicknameLabel = new JLabel("昵    称：");
		nicknameTextField = new JTextField();

		passwordLabel = new JLabel("密    码：");
		passwordField = new JPasswordField();

		confirmPasswordLabel = new JLabel("确认密码：");
		confirmPasswordField = new JPasswordField();

		resetButton = new JButton("重置");
		registerButton = new JButton("注册");
	}

	private void setLayout() {
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(titleLabel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
				.addComponent(messageLabel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(nameLabel, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(nameTextField, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE).addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(nicknameLabel, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(nicknameTextField, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
						.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(passwordLabel, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(passwordField, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE).addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(confirmPasswordLabel, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(confirmPasswordField, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
						.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(resetButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
						.addComponent(registerButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
						.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addComponent(titleLabel, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE).addGap(18)
				.addComponent(messageLabel).addGap(18)
				.addGroup(groupLayout
						.createParallelGroup(Alignment.BASELINE).addComponent(nameLabel).addComponent(nameTextField,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(nicknameLabel).addComponent(
						nicknameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(groupLayout
						.createParallelGroup(Alignment.BASELINE).addComponent(passwordLabel).addComponent(passwordField,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(confirmPasswordLabel)
						.addComponent(confirmPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, Short.MAX_VALUE).addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(resetButton).addComponent(registerButton))
				.addGap(87)));
		getContentPane().setLayout(groupLayout);
	}

	private void addListener() {
		resetButton.setActionCommand("Reset");
		resetButton.addActionListener(new RegisterMonitor(this));

		registerButton.setActionCommand("Register");
		registerButton.addActionListener(new RegisterMonitor(this));

		nameTextField.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				setMessage("");
			}
		});

		nicknameTextField.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				setMessage("");
			}
		});

		passwordField.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				setMessage("");
			}
		});

		confirmPasswordField.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				setMessage("");
			}
		});

		nameTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					nicknameTextField.grabFocus();
				}
			}
		});

		nicknameTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					passwordField.grabFocus();
				}
			}
		});

		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					confirmPasswordField.grabFocus();
				}
			}
		});

		confirmPasswordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					registerButton.doClick();
				}
			}
		});
	}
}
