package org.zero.process;

import java.net.Socket;

import javax.swing.JFrame;

import org.zero.entity.Response;

public interface Process {
	void processing(Response response, Socket socket, JFrame jFrame);
}
