package org.zero.process;

import java.awt.EventQueue;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.zero.entity.Response;
import org.zero.util.Connection;
import org.zero.view.LoginWindow;
import org.zero.view.RegisterWindow;

public class RegisterProcess implements Process {

	public void processing(Response response, Socket socket, JFrame jFrame) {
		if (!(jFrame instanceof RegisterWindow)) {
			throw new IllegalArgumentException("请传入 RegisterWindow 类型的 JFrame");
		}
		RegisterWindow registerWindow = (RegisterWindow) jFrame;

		if (response.getResponseCode() == Response.REGISTER_FAIL) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(registerWindow, "注册失败！！！\n请重试", "注册失败",
					JOptionPane.INFORMATION_MESSAGE));
			registerWindow.setMessage("");
			registerWindow.setName("");
			registerWindow.setNickname("");
			registerWindow.setPassword("");
			registerWindow.setConfirmPassword("");
			return;
		}

		if (response.getResponseCode() == Response.REGISTER_SUCCESS) {
			registerWindow.setMessage("已为您生成随机QQ号【" + response.getToUser().getId() + "】。请您牢记，以便今后登录使用！！！");
			int result = JOptionPane.showConfirmDialog(registerWindow, "恭喜您，注册成功！请牢记QQ号！！！\n是否登录？", "提示消息",
					JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				registerWindow.dispose();
				EventQueue.invokeLater(() -> {
					new LoginWindow(response.getToUser().getId());
				});
			} else {
				Connection.close();
				System.exit(0);
			}
		}
	}

}
