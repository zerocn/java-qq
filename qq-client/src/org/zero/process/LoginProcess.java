package org.zero.process;

import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.zero.entity.Response;
import org.zero.view.LoginWindow;
import org.zero.view.MainWindow;

public class LoginProcess implements Process {

	@Override
	public void processing(Response response, Socket socket, JFrame jFrame) {

		if (!(jFrame instanceof LoginWindow)) {
			throw new IllegalArgumentException("请传入 LoginWindow 类型的 JFrame");
		}
		LoginWindow loginWindow = (LoginWindow) jFrame;

		if (response.getResponseCode() == Response.LOGIN_USERNAME_INVALID) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(loginWindow, "QQ 号不存在！！！\n请先注册", "错误",
					JOptionPane.INFORMATION_MESSAGE));
			loginWindow.setId("");
			loginWindow.setPassword("");
			return;
		}

		if (response.getResponseCode() == Response.LOGIN_PASSWORD_ERROR) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(loginWindow, "密码错误！请重新输入", "错误",
					JOptionPane.INFORMATION_MESSAGE));
			loginWindow.setPassword("");
			return;
		}

		if (response.getResponseCode() == Response.LOGIN_SUCCESS) {
			loginWindow.dispose();
			new MainWindow(response.getToUser());
		}
	}

}
