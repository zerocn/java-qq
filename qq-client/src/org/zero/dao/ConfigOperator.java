package org.zero.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class ConfigOperator {
	private static File systemConfig;
	private static Properties configurationItems = new Properties();

	static {
		// 获取文件
		systemConfig = new File(ConfigOperator.class.getResource("config.properties").getFile().replace("%20", " "));

		try (FileInputStream fileInputStream = new FileInputStream(systemConfig)) {
			configurationItems.load(fileInputStream);
		} catch (FileNotFoundException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
					"未找到config.properties文件\n" + e.getMessage(), "错误", JOptionPane.ERROR_MESSAGE));
		} catch (IOException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
					"读取config.properties文件出错\n" + e.getMessage(), "错误", JOptionPane.ERROR_MESSAGE));
		}
	}

	public static String getIp() {
		try {
			return configurationItems.getProperty("ip", "127.0.0.1");
		} catch (Exception e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
					"读取配置项【ip】出错，自动设置为 localhost\n" + e.getMessage(), "错误", JOptionPane.INFORMATION_MESSAGE));
			return "localhost";
		}
	}

	public static int getPort() {
		try {
			return Integer.parseInt(configurationItems.getProperty("port", "8888"));
		} catch (Exception e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
					"读取配置项【port】出错，自动设置为8888\n" + e.getMessage(), "错误", JOptionPane.INFORMATION_MESSAGE));
			return 8888;
		}
	}

	public static String getProcessClass(String processName) {
		try {
			return configurationItems.getProperty(processName, "exit");
		} catch (Exception e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
					"读取配置项【" + processName + "】出错，自动设置为 Exit\n" + e.getMessage(), "错误",
					JOptionPane.INFORMATION_MESSAGE));
			return "org.zero.process.ExitProcess";
		}
	}
}
