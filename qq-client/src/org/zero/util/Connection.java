package org.zero.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Objects;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.zero.dao.ConfigOperator;
import org.zero.entity.Request;
import org.zero.entity.Response;
import org.zero.process.Process;

public class Connection {

	private static Socket socket;

//	static {
//		String ip = ConfigOperator.getIp();
//		int port = ConfigOperator.getPort();
//		try {
//			socket = new Socket(ip, port);
//		} catch (IOException e) {
//			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "创建 Socket 错误！\n" + e.getMessage(),
//					"错误", JOptionPane.ERROR_MESSAGE));
//		}
//	}

	private static void setSocket() {
		if (Objects.isNull(socket) || socket.isClosed()) {
			try {
				String ip = ConfigOperator.getIp();
				int port = ConfigOperator.getPort();
				socket = new Socket(ip, port);
			} catch (IOException e) {
				SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "创建 Socket 错误！\n" + e.getMessage(),
						"错误", JOptionPane.ERROR_MESSAGE));
			}
		}
	}

	public static void send(Request request) {
		setSocket();
		ObjectOutputStream objectOutputStream = null;
		try {
			objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			objectOutputStream.writeObject(request);
			objectOutputStream.flush();
		} catch (IOException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "读取输入流信息出错！\n" + e.getMessage(), "错误",
					JOptionPane.ERROR_MESSAGE));
		} catch (NullPointerException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null,
					"创建输出流出错！\n可能是服务器未开启\n" + e.getMessage(), "错误", JOptionPane.ERROR_MESSAGE));
		}
//		finally {
//			try {
//				objectOutputStream.close();
//			} catch (Exception e) {
//				SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "关闭输出流错误！\n" + e.getMessage(),
//						"错误", JOptionPane.ERROR_MESSAGE));
//			}
//
//		}
	}

	public static void receive(JFrame jFrame) {
		ObjectInputStream objectInputStream = null;
		try {
			objectInputStream = new ObjectInputStream(socket.getInputStream());
			Response response = (Response) objectInputStream.readObject();
			String responseServiceName = response.getResponseServiceName().toLowerCase();
			// 从文件获取 process 全限定类名
			String className = ConfigOperator.getProcessClass(responseServiceName);
			// 创建 Process 对象
			Process process = (Process) Class.forName(className).getDeclaredConstructor().newInstance();
			process.processing(response, socket, jFrame);
		} catch (IOException e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(jFrame,
					"服务连接错误！\n可能是服务器的问题！\n请稍后重试\n" + e.getMessage(), "错误", JOptionPane.WARNING_MESSAGE));
		} catch (Exception e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(jFrame, "信息处理错误！\n" + e.getMessage(), "错误",
					JOptionPane.ERROR_MESSAGE));
		}
//		finally {
//			try {
//				objectInputStream.close();
//			} catch (Exception e) {
//				SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(jFrame, "关闭输入流错误！\n" + e.getMessage(),
//						"错误", JOptionPane.ERROR_MESSAGE));
//			}
//		}
	}

	public static void close() {
		try {
			socket.close();
		} catch (Exception e) {
			SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "关闭 Socket 错误！\n" + e.getMessage(),
					"错误", JOptionPane.ERROR_MESSAGE));
		}
	}
}
