package org.zero.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Request implements Serializable {
	private static final long serialVersionUID = -5257759343427241897L;

	private String requestServiceName;

	private User fromUser;
	private User toUser;
	private String message;
}
